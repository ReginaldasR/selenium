import com.google.common.io.Files
import org.junit.rules.MethodRule
import org.junit.runners.model.FrameworkMethod
import org.junit.runners.model.Statement
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver

class ScreenshotTestRule implements MethodRule {

    WebDriver testDriver

    public ScreenshotTestRule(WebDriver driver) {
        this.testDriver = driver
    }

    public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, final Object o) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    statement.evaluate()
                } catch (Throwable t) {
                    captureScreenshot(frameworkMethod.getName().replaceAll(" ", "-"))
                    throw t
                }
            }

            public void captureScreenshot(String fileName) {
                try {
                    new File("/Users/reginaldas/screens/test-screenshots/").mkdirs()
                    File  screenshot = ((TakesScreenshot) testDriver).getScreenshotAs(OutputType.FILE)
                    String imagePath = "/Users/reginaldas/screens/test-screenshots/" + fileName + "-screenshot.png";
                    Files.copy(screenshot, new File(imagePath))
                } catch (Exception e) {
                    print "Error while creating screenshot " + fileName
                }
            }
        }
    }
}
