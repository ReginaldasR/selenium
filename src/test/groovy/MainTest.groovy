import org.junit.Rule
import org.junit.rules.MethodRule
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.support.ui.Select
import spock.lang.Shared
import spock.lang.Specification

class MainTest extends Specification {
    private final String USER_NAME = "Brigita Brigita"
    @Shared
    private WebDriver driver = new FirefoxDriver();
    @Rule
    public MethodRule rule = new ScreenshotTestRule(driver)
    private String baseUrl = "http://81.7.118.186/dms9/#"

//    void setupSpec() {
//        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
//                "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome")
//        println System.getProperty('webdriver.chrome.driver')
//    }

    def cleanupSpec() {
        driver.close()
    }

    def "test if we can login to dms"() {
        when:
            driver.get(baseUrl + '/login')
            Thread.sleep(500)
            WebElement select = driver.findElementById('selected-user')
            Select dropDown = new Select(select);
            dropDown.selectByVisibleText(USER_NAME)
            driver.findElementByXPath("//form/button[contains(text(),'Prisijungti')]").click()
        then:
            Thread.sleep(500)
            def authenticatedUserName = driver.findlementByXPath("//p[@ng-bind='authenticatedPersonName']").getText()
            authenticatedUserName.equals(USER_NAME)
    }

    def "Brigita Brigita should have more then one project"() {
        when:
            driver.get(baseUrl + '/projects')
        then:
            int rowCount = driver.findElements(By.xpath("//table[@ng-table='projectsTableParams']/tbody/tr")).size()
            rowCount > 1
    }

}
